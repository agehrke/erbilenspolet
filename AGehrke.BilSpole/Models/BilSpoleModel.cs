﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AGehrke.BilSpole.Models
{
    public class BilSpoleModel
    {        
        [Required]
        public string RegNr { get; set; }

        [Required]
        public int Km { get; set; }

        public bool? Spolet { get; set; }
        public IEnumerable<Service.TrafikstyrelseResult> Registrations { get; set; }
        public int MaxKm { get; set; }
    }
}