﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using AGehrke.BilSpole.Service;
using AGehrke.BilSpole.Models;

namespace AGehrke.BilSpole.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public Task<ActionResult> Check(string regnr, int km)
        {
            if (string.IsNullOrWhiteSpace(regnr)) return Task.Factory.FromResult<ActionResult>(new HttpNotFoundResult("Missing regnr"));

            var ajaxRequest = Request.IsAjaxRequest();

            regnr = regnr.Replace(" ", string.Empty).Trim();

            var service = new BilSpoleService();
            var task = service.GetInfoAsync(regnr);
            return task.ContinueWith<ActionResult>(resultTask =>
            {
                if (resultTask.IsFaulted && resultTask.Exception.GetBaseException() is UnknownRegistrationNumberException)
                {
                    return new HttpNotFoundResult("Ukendt registreringsnummer");
                }
                else if (resultTask.IsFaulted)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(resultTask.Exception.Flatten());
                    return new HttpStatusCodeResult(500, "Der opstod en fejl. Prøv igen");
                }

                var result = resultTask.Result;
                var model = new BilSpoleModel
                {
                    RegNr = regnr,
                    Km = km,
                    Spolet = service.ContainsOdometerFraud(result, km),
                    MaxKm = service.GetMaxRegistredKm(result),
                    Registrations = result
                };

                if (ajaxRequest)
                {
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return View("Index", model);
                }
            });
        }
    }
}
