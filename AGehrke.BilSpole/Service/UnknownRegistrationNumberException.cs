﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGehrke.BilSpole.Service
{
    [Serializable]
    public class UnknownRegistrationNumberException : Exception
    {
        public UnknownRegistrationNumberException() { }
        public UnknownRegistrationNumberException(string message) : base(message) { }
        public UnknownRegistrationNumberException(string message, Exception inner) : base(message, inner) { }
        protected UnknownRegistrationNumberException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}