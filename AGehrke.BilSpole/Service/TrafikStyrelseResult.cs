﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGehrke.BilSpole.Service
{
    public class TrafikstyrelseResult
    {
        public string ReportUrl { get; set; }

        public string RegNr { get; set; }

        public int Km { get; set; }

        public string Status { get; set; }

        public DateTime Date { get; set; }
    }
}