﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net;
using HtmlAgilityPack;

namespace AGehrke.BilSpole.Service
{
    public class BilSpoleService
    {
        public Task<IEnumerable<TrafikstyrelseResult>> GetInfoAsync(string regNrOrVin)
        {
            var webClient = new WebClient();
            webClient.Encoding = System.Text.Encoding.UTF8;
            var task = webClient.DownloadStringTask(CreateUri(regNrOrVin));
            return task.Then(html =>
            {
                webClient.Dispose();
                return ParseHtml(regNrOrVin, html);
            });
        }

        public bool ContainsOdometerFraud(IEnumerable<TrafikstyrelseResult> registrations, int currentKilometer)
        {
            // Simple check on max km
            return GetMaxRegistredKm(registrations) > currentKilometer;
        }

        public int GetMaxRegistredKm(IEnumerable<TrafikstyrelseResult> registrations)
        {
            return registrations.Max(r => r.Km);
        }

        private static IEnumerable<TrafikstyrelseResult> ParseHtml(string regNrOrVin, string html)
        {
            // Parse HTML using HtmlAgilityPack
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(html);

            var results = new List<TrafikstyrelseResult>();
            var nodes = htmlDoc.DocumentNode.SelectNodes("//table[@id=\"tblInspections\"]/tbody/tr");
            if (nodes == null || nodes.Count == 0) throw new UnknownRegistrationNumberException(regNrOrVin + " is unknown");

            foreach (var node in nodes)
            {
                var result = CreateResult(node);
                results.Add(result);
            }

            return results;
        }

        private static TrafikstyrelseResult CreateResult(HtmlNode node)
        {
            var tds = node.SelectNodes("td");
            var date = tds[0].InnerText;
            var status = tds[1].InnerText;
            var km = tds[2].InnerText;
            var reg = tds[3].InnerText;
            var reportUrl = tds[4].SelectSingleNode("a").GetAttributeValue("href", string.Empty);

            var result = new TrafikstyrelseResult
            {
                Date = DateTime.Parse(date, System.Globalization.CultureInfo.GetCultureInfo("da")),
                Status = status,
                Km = int.Parse(km.Replace(".", "")),
                RegNr = reg,
                ReportUrl = reportUrl
            };
            return result;
        }

        private static Uri CreateUri(string regNrOrVin)
        {
            if (regNrOrVin.Length < 10) return new Uri("http://selvbetjening.trafikstyrelsen.dk/Sider/resultater.aspx?Reg=" + regNrOrVin);
            else return new Uri("http://selvbetjening.trafikstyrelsen.dk/Sider/resultater.aspx?Vin=" + regNrOrVin);
        }
    }
}